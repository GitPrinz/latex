# credit to https://gitlab.com/martisak/latex-pipeline

SOURCES=$(wildcard ./*/*main.tex)  # find "...main.tex" files in all subfolders
PDF_OBJECTS=$(SOURCES:.tex=.pdf)  # create list with pdf targets

# Options #
###########

LATEXMK=latexmk
LATEXMK_OPTIONS=-f -bibtex -pdf -pdflatex="pdflatex -interaction=nonstopmode"

DOCKER=docker
DOCKER_COMMAND=run --rm -w /data/ --env LATEXMK_OPTIONS_EXTRA=$(LATEXMK_OPTIONS_EXTRA)
DOCKER_MOUNT=-v`pwd`:/data

all: render

pdf: $(PDF_OBJECTS)

%.pdf: %.tex  # all pdf targets depend on their tex file.
	@echo Input file: $<
	cd $(dir $<) &&	$(LATEXMK) $(LATEXMK_OPTIONS_EXTRA) $(LATEXMK_OPTIONS) $(notdir $<)

clean:
	-$(LATEXMK) -bibtex -C main
	-make -C figures clean

dist-clean: clean
	-rm $(FILENAME).tar.gz

render:
	$(DOCKER) $(DOCKER_COMMAND) $(DOCKER_MOUNT) blang/latex:ctanfull \
		make pdf

figures:
	$(DOCKER) $(DOCKER_COMMAND) $(DOCKER_MOUNT) python:3.8 \
		make -C /data/figures

debug:
	$(DOCKER) $(DOCKER_COMMAND) -it $(DOCKER_MOUNT) blang/latex:ctanfull \
		bash

check_docker:
	$(DOCKER) $(DOCKER_COMMAND) -it $(DOCKER_MOUNT) ruby:2.7.1 \
		bundle update --bundler; make check

.PHONY: figures
