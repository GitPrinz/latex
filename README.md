# Simple LaTeX Drafts

Created for my needs. Feel free to comment.

See 

- [example_document.tex](example_document/example_document_main.tex)  
  [![PDF](https://img.shields.io/badge/Download-PDF-green)](https://gitlab.com/GitPrinz/latex/-/jobs/artifacts/master/raw/example_document/example_document_main.pdf?job=compile) (master)
- [example_poster.tex](example_poster/example_poster_main.tex)  
  [![PDF](https://img.shields.io/badge/Download-PDF-green)](https://gitlab.com/GitPrinz/latex/-/jobs/artifacts/master/raw/example_poster/example_poster_main.pdf?job=compile) (master)
- [example_presentation.tex](example_presentation/example_presentation_main.tex)  
  [![PDF](https://img.shields.io/badge/Download-PDF-green)](https://gitlab.com/GitPrinz/latex/-/jobs/artifacts/master/raw/example_presentation/example_presentation_main.pdf?job=compile) (master)

See also

- [LaTeX Paper](https://gitlab.com/GitPrinz/latex-paper)

## Experience

### Poster
It sucks if you have a special idea of how it should look.  
I would prefere PowerPoint for a quick and good result.

## Automatic PDF creation

 ![Pipeline status](https://gitlab.com/GitPrinz/latex/badges/master/pipeline.svg) (master)

This Repository contains a CI to automatically create all the pdf documents.

Thanks very much to [Martin Isaksson](https://gitlab.com/martisak) for the [Code](https://gitlab.com/martisak/latex-pipeline)!
Also to [Gerard Rozsavolgyi](https://gitlab.com/roza) for the [Badge Idea](https://gitlab.com/roza/latex-pipeline/).

##  Steps to your own version
- delete irrelevant parts  
  Every "main" file should be created
- maybe check [Makefile](Makefile)